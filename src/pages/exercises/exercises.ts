import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import {ModalController} from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';

@Component({
  selector: 'page-exercises',
  templateUrl: 'exercises.html'
})
export class ExerciseListPage {
   params: Object;
  public exercises: Array<{exerciseName: string, primaryMuscle: string}>;
   //dataService: DataProvider;

    //public exercises = [];
  constructor(public navCtrl: NavController, public view: ViewController, public modalCtrl:ModalController, public dataService: DataProvider) { 
    this.dataService.getData().then((exerciseItems) => {
    
        if(exerciseItems){
            this.exercises = exerciseItems;
        } 

//else {
  //      this.exercises =
  //  [{exerciseName: 'test name 1', primaryMuscle: 'muscle 1'},
  //  {exerciseName: 'test name 2', primaryMuscle: 'muscle 2'}];
  //      }
    });
  }

    
    ionViewDidLoad(){
      //this.exercises;
    }
    
    addItem() {
    let addModal = this.modalCtrl.create('AddExercisePage');
    
    addModal.onDidDismiss((item) => {
        if(item){
        this.saveItem(item);
        }
    });
    
    addModal.present();
    
    }
    
    saveItem(item){
    this.exercises.push(item);
    this.dataService.save(this.exercises);
    alert(this.exercises);
    }
    
    
  
  openAddExercisePage() {
    this.navCtrl.push('AddExercisePage');
  }
  

}
