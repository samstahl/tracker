import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddExercisePage } from './addExercise';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

@NgModule({
declarations: [
    AddExercisePage,
],
imports: [
    IonicPageModule.forChild(AddExercisePage),
],
exports: [
    AddExercisePage,
    AddExercisePageModule,
],
providers: [

]
})

export class AddExercisePageModule{}