import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import {  NavController, NavParams, ViewController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-add-exercises',
  templateUrl: 'addExercise.html'
})
export class AddExercisePage {
 
exerciseName: string;
primaryMuscle: string;


  constructor(public navCtrl: NavController, public view: ViewController) {
   
}
  
  
  close() {
   this.view.dismiss();
  }

  addItem() {  
    let newItem = {
        exerciseName: this.exerciseName,
        primaryMuscle: this.primaryMuscle
    };
    
    alert(this.exerciseName);
    
    this.view.dismiss(newItem);
  }
  
  
}
